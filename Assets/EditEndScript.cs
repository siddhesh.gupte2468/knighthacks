﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditEndScript : MonoBehaviour {

    public InputField mainInputField;
    public GameObject APIHandlerGO;
    public GameObject changeVideoObj;

    void ProcessSingle(string s)
    {
        print("ProcessSingle " + s);
        changeVideoObj.GetComponent<VideoChanger>().video_index = ParseInt(s);

    }

    public static int ParseInt(string value, int defaultValue = 0)
    {
        int parsedInt;
        if (int.TryParse(value, out parsedInt))
        {
            return parsedInt;
        }

        return defaultValue;
    }
    void sendToAPIHandler(InputField input)
    {
        //if (input.text.ToLower().Contains("tyre"))
        //{
        //    changeVideoObj.GetComponent<VideoChanger>().video_index = 2;
        //}
        //else if (input.text.ToLower().Contains("cpr"))
        //{
        //    changeVideoObj.GetComponent<VideoChanger>().video_index = 0;

        //}
        //else if (input.text.ToLower().Contains("french toast"))
        //{
        //    changeVideoObj.GetComponent<VideoChanger>().video_index = 5;

        //}
        //else if (input.text.ToLower().Contains("pancakes"))
        //{
        //    changeVideoObj.GetComponent<VideoChanger>().video_index = 6;

        //}
        //else if (input.text.ToLower().Contains("tie"))
        //{
        //    changeVideoObj.GetComponent<VideoChanger>().video_index = 10;

        //}

        APIHandlerGO.GetComponent<APIHandler>().getIndexForQuestion(input.text, ProcessSingle);
    }

	// Use this for initialization
	void Start () {
        mainInputField.onEndEdit.AddListener(delegate { sendToAPIHandler(mainInputField); });
    }
	
}
