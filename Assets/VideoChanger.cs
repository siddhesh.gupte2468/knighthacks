﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoChanger : MonoBehaviour {

    //do a +2 to the index

    public int video_index;
    public VideoClip[] video_lst;

    public VideoPlayer target_videoplayer;

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        target_videoplayer.clip = video_lst[video_index + 2];    	
	}
}
