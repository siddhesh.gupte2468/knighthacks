﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class APIHandler : MonoBehaviour {

    //string server_url = "http://127.0.0.1:8000/";
    //string server_url = "http://siddheshgupte1.pythonanywhere.com/";
    string server_url = "http://11f7c818.ngrok.io/";

    public delegate void WWWDataParseMethod(string s);
    public GameObject changeVideoObj;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static int ParseInt(string value, int defaultValue = 0)
    {
        int parsedInt;
        if(int.TryParse(value, out parsedInt))
        {
            return parsedInt;
        }

        return defaultValue;
    }

    void ProcessSingle(string s)
    {
        print("ProcessSingle " + s);
        changeVideoObj.GetComponent<VideoChanger>().video_index = ParseInt(s);

    }

    public void getIndexForQuestion(string question, WWWDataParseMethod ProcessSingle)
    {
        StartCoroutine(MakeGetReq("question/?question=" + question, ProcessSingle));
    }

    IEnumerator MakeGetReq(string suffix, WWWDataParseMethod textout)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(server_url + suffix))
        {
            Debug.Log(server_url + suffix);
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                yield return www.downloadHandler;
                Debug.Log(www.downloadHandler.text);

                byte[] results = www.downloadHandler.data;

                textout(www.downloadHandler.text);
            }
        }
    }
}
